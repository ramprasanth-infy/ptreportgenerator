/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.infy.starter;

import com.infy.helper.FileOperation;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author ram
 */
public class PerformanceReportGenerator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        if(args.length<0)
        {
             System.err.println("Execute the program with proper params. Ex: java -jar PTReport.jar inputfilename outputfilename. complete path of input and output file should be given. Ex: java -jar PTReport.jar /home/dev/Desktop/input.txt /home/dev/Desktop/report1.xls"); 
        }
        else if(args.length<1)
        {
            System.err.println("Execute the program with proper params. Ex: java -jar PTReport.jar inputfilename outputfilename. complete path of input and output file should be given. Ex: java -jar PTReport.jar /home/dev/Desktop/input.txt /home/dev/Desktop/report1.xls"); 
        }
        else if(args.length >2)
        {
            System.err.println("Execute the program with proper params. Ex: java -jar PTReport.jar inputfilename outputfilename. complete path of input and output file should be given. Ex: java -jar PTReport.jar /home/dev/Desktop/input.txt /home/dev/Desktop/report1.xls"); 
        }
        else if(args.length==2)
        {
                String filePath=args[0];
                String outputFile=args[1];
                ArrayList<String> jtlFileList =FileOperation.readInputFileAndReturnJTLFileList(filePath);

                if(jtlFileList==null)
                {
                  System.err.println("Check JTL File path and name");   
                }
                else
                {
                    Map<String,ArrayList<Double>> data=FileOperation.readJTLFile(jtlFileList);
                    FileOperation.createExcelReport(data,outputFile);
                }

                
        }
    }
    
}
