/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.infy.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author ram
 */
public class FileOperation {
    
    
    public static Map<String,ArrayList<Double>> readJTLFile(ArrayList<String> files)
    {
        Map<String,ArrayList<Double>> data=new LinkedHashMap<>();
        String fileName="";
        try {
            for(int i=0;i<files.size();i++)
            {
                fileName=files.get(i);
                System.out.println("Processing--"+fileName);
                File file = new File(fileName);
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
                ArrayList<Double> responseTime=new ArrayList<Double>();
                String key="";
                while ((line = bufferedReader.readLine()) != null) {
			String[] individualData=line.split(",");
                        double seconds = Integer.parseInt(individualData[1]) / 1000.0;
                        key=individualData[2];
                        responseTime.add(seconds);
		}
                fileReader.close();
                data.put(key,responseTime);        
                        
            }
            
            return data;
          } 
          catch (IOException e) 
          {
			//System.out.println("Read JTL file Function"+e.toString());
                        System.out.println("********* Check path of "+fileName+"********");
                        return null;
          }
    }
    
    public static ArrayList<String> readInputFileAndReturnJTLFileList(String filePath)
    {
        try {
                File file = new File(filePath);
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
                ArrayList<String> fileList=new ArrayList<String>();
		String line;
                ArrayList<Double> responseTime=new ArrayList<Double>();
                String key="";
                while ((line = bufferedReader.readLine()) != null) {
			fileList.add(line.trim());
		}
                
            return fileList;
          } 
          catch (IOException e) 
          {
			System.out.println("jtlFilePathFromInputFile Function"+e.toString());
                        return null;
          }
    }
    
    public static ArrayList<String> getLeftSideHeader()
    {
        ArrayList<String> leftSideHeader=new ArrayList<>();
        
        leftSideHeader.add("Less Than 0.2 sec");
        leftSideHeader.add("Less Than 0.5 sec");
        leftSideHeader.add("Less Than 1.0 sec");
        leftSideHeader.add("Less Than 1.5 sec");
        leftSideHeader.add("Greater Than 0.2 sec");
        leftSideHeader.add("Greater Than 0.5 sec");
        leftSideHeader.add("Greater Than 1.0 sec");
        leftSideHeader.add("Greater Than 1.5 sec");
        leftSideHeader.add("Between 0.2 sec and 0.5 sec");
        leftSideHeader.add("Between 0.5 sec and 1.0 sec");
        leftSideHeader.add("Between 1.0 sec and 1.5 sec");
        return leftSideHeader;
    }
    
    public static void createExcelReport(Map<String,ArrayList<Double>> data,String outputFile)
    {
        System.out.println("Started creating report");
        ArrayList<String> leftSideHeader=getLeftSideHeader();
        Set<String> keyset = data.keySet();
        int rownum = 0;
       try
       {
           FileOutputStream fileOut = new FileOutputStream(outputFile);
       HSSFWorkbook workbook = new HSSFWorkbook();
        for (String key : keyset) 
        {
            HSSFSheet worksheet = workbook.createSheet(key);
            
            ArrayList<Double> responseTime=data.get(key);
            
            int totalNoRequestsProcessed=responseTime.size();
//            int noOfRequestsLessThanPointTwoSecond=0; // < 0.2
//            int noOfRequestsLessThanPointFiveSecond=0;  //<0.5
//            int noOfRequestsLessThanOneSecond=0;  // <1.0 
//            int noOfRequestsLessThanOnePointFiveSecond=0; // <1.5
//            
//            int noOfRequestsGreaterThanPointTwoSecond=0; // > 0.2
//            int noOfRequestsGreaterThanPointFiveSecond=0; // > 0.5
//            int noOfRequestsGreaterThanOneSecond=0; // > 1.0
//            int noOfRequestsAboveOnePointFiveSecond=0; // >1.5
//           
//            int noOfRequestsLessThanPointTwoAndPointFiveSecond=0;  //0.2> && <0.5
//            int noOfRequestsBetweePointFiveAndOneSecond=0; // >0.5 && <1.0
//            int noOfRequestBetweenOneSecondAndOnePointFiveSecond=0; // >1.0 && <1.5
            
            double [] requestTracker=new double[]{0,0,0,0,0,0,0,0,0,0,0};
            
            for(int i=0;i<totalNoRequestsProcessed;i++)
            {
                Double sec=responseTime.get(i);
                if(sec < 0.2)
                {
                   // noOfRequestsLessThanPointTwoSecond++;
                    requestTracker[0]++;
                }
                if(sec < 0.5)
                {
                    //noOfRequestsLessThanPointFiveSecond++;
                    requestTracker[1]++;
                }
                if(sec < 1.0)
                {
                    //noOfRequestsLessThanOneSecond++;
                    requestTracker[2]++;
                }
              
                if(sec < 1.5)
                {
                    //noOfRequestsLessThanOnePointFiveSecond++;
                    requestTracker[3]++;
                }
                if(sec >= 0.2)
                {
                    //noOfRequestsGreaterThanPointTwoSecond++;
                    requestTracker[4]++;
                }
                
                if(sec >= 0.5)
                {
                    //noOfRequestsGreaterThanPointTwoSecond++;
                    requestTracker[5]++;
                }
                if(sec >=1.0)
                {
                    //noOfRequestsGreaterThanOneSecond++;
                    requestTracker[6]++;
                }
                if(sec >1.5)
                {
                    //noOfRequestsAboveOnePointFiveSecond++;
                    requestTracker[7]++;
                }
                if(sec >0.2 && sec <0.5)
                {
                    //noOfRequestsLessThanPointTwoAndPointFiveSecond++;
                    requestTracker[8]++;
                }
                if(sec > 0.5 && sec <1.0)
                {
                    //noOfRequestsBetweePointFiveAndOneSecond++;
                    requestTracker[9]++;
                }
                if(sec > 1.0 && sec <1.5)
                {
                    //noOfRequestBetweenOneSecondAndOnePointFiveSecond++;
                    requestTracker[10]++;
                }
              
              
            }
            
            
            int lHSize=leftSideHeader.size();
            int row=0;
             HSSFRow trow = worksheet.createRow((short) 0);
                        HSSFCell cellA = trow.createCell((short) 0);
			cellA.setCellValue("Total Request");
			HSSFCellStyle cellStyle1 = workbook.createCellStyle();
			cellStyle1.setFillForegroundColor(HSSFColor.GOLD.index);
			cellStyle1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			cellA.setCellStyle(cellStyle1);

			HSSFCell cellB = trow.createCell((short) 1);
			cellB.setCellValue(totalNoRequestsProcessed);
			cellStyle1 = workbook.createCellStyle();
			cellStyle1.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
			cellStyle1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			cellB.setCellStyle(cellStyle1);
            
            for(int i=0;i<lHSize;i++)
            {    
                        HSSFRow row1 = worksheet.createRow((short) i+1);
                        HSSFCell cellA1 = row1.createCell((short) 0);
			cellA1.setCellValue(leftSideHeader.get(i));
			HSSFCellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
			cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			cellA1.setCellStyle(cellStyle);

			HSSFCell cellB1 = row1.createCell((short) 1);
			cellB1.setCellValue(requestTracker[i]);
			cellStyle = workbook.createCellStyle();
			cellStyle.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
			cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			cellB1.setCellStyle(cellStyle);
                        
                        
                        //System.out.println(requestTracker[i]+"/"+totalNoRequestsProcessed+"="+requestTracker[i]/totalNoRequestsProcessed);
                        
                        HSSFCell cellC1 = row1.createCell((short) 2);
			cellC1.setCellValue((requestTracker[i]/totalNoRequestsProcessed)*100.0);
			cellStyle = workbook.createCellStyle();
			cellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
			cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			cellC1.setCellStyle(cellStyle);
                        
            }        
            
        }
           workbook.write(fileOut);
			fileOut.flush();
			fileOut.close();
                        
         System.out.println("Completed !. Check report file at "+outputFile);
       }
       catch(Exception e)
       {
           System.out.println("Create Excel Function"+e.toString());
       }
       
    }
    
}
